package com.harish.kaplanassignment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.harish.kaplanassignment.R
import com.harish.kaplanassignment.model.ListAll
import com.harish.kaplanassignment.utils.Utils
import com.squareup.picasso.Picasso

internal class FeedsAdapter(listAll: ArrayList<ListAll>, context: Context) :
    RecyclerView.Adapter<FeedsAdapter.MyViewHolder>() {

    var listAll: ArrayList<ListAll>? = null
    var context: Context? = null

    init {
        this.listAll = listAll
        this.context = context
    }

    fun updateList(listAll: ArrayList<ListAll>) {
        this.listAll = listAll
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = listAll?.get(position)
        holder.textView?.text = item?.weather?.get(0)?.main
        holder.dayText?.text = Utils.parseDateToddMMyyyy(item?.dtTxt!!)
        Picasso.get().load(Utils.getImageUrl(item?.weather?.get(0)?.icon!!)).into(holder.image)
    }

    override fun getItemCount(): Int {
        return listAll!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView? = null
        var textView: TextView? = null
        var dayText: TextView? = null

        init {
            image = itemView.findViewById(R.id.imageView)
            textView = itemView.findViewById(R.id.text_view)
            dayText = itemView.findViewById(R.id.day_cloud)
        }
    }
}