package com.harish.kaplanassignment.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object {
        val AUTOCOMPLETE_REQUEST_CODE = 1000
        val ZOOM = 15f
        val search = "Search here..."
        val apiKey = "8f5c81622b11501198cb68ce1dcece20"

        fun getImageUrl(value: String): String {
            return "http://openweathermap.org/img/w/" + value + ".png"
        }

        @SuppressLint("SimpleDateFormat")
        fun parseDateToddMMyyyy(time: String): String? {
            val inputPattern = "yyyy-MM-dd HH:mm:ss"
            val outputPattern = "dd MMM HH:mm"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date: Date? = null
            var str: String? = null
            try {
                date = inputFormat.parse(time)
                str = outputFormat.format(date!!)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str
        }
    }
}