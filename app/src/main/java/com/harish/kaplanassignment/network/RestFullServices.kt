package com.harish.sportskeeda.network

import com.google.android.gms.maps.model.LatLng
import com.harish.kaplanassignment.model.MainList
import com.harish.kaplanassignment.ui.IMain
import com.harish.kaplanassignment.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestFullServices {

    companion object {
        private val url = "http://api.openweathermap.org/"

        private fun getRetrofitClient(): ApiManager {
            val retrofit = Retrofit.Builder().baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val manager = retrofit.create(ApiManager::class.java)
            return manager
        }

        fun makeFeedRequest(callback: IMain.IMainViewModel, latLng: LatLng) {
            getRetrofitClient().getFeeds(latLng.latitude.toString(), latLng.longitude.toString(), Utils.apiKey).enqueue(object : Callback<MainList> {
                override fun onResponse(call: Call<MainList>, response: Response<MainList>) {
                    if (response.isSuccessful) {
                        callback.whetherCallBack(response.body()!!)
                    } else {
                        callback.onCompletedWithError(response.message())
                    }
                }

                override fun onFailure(call: Call<MainList>, t: Throwable) {
                    callback.onError(t.message!!)
                }
            })
        }
    }

}