package com.harish.sportskeeda.network

import com.harish.kaplanassignment.model.MainList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiManager {
    @GET("data/2.5/forecast?")
    fun getFeeds(@Query("lat") lat: String,
                 @Query("lon") lon: String,
                 @Query("appid") appid: String): Call<MainList>
}