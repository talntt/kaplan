package com.harish.kaplanassignment.ui

import android.view.View
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.harish.kaplanassignment.R
import com.harish.kaplanassignment.model.MainList
import com.harish.kaplanassignment.utils.Utils
import com.harish.sportskeeda.network.RestFullServices

class MapsViewModel: ViewModel(),OnMapReadyCallback, IMain.IMainViewModel {

    var mIMainView: IMain.IMainView? = null
    var city: ObservableField<String> = ObservableField()
    var temparature: ObservableField<String> = ObservableField()
    var data: MutableLiveData<MainList>? = MutableLiveData()
    var mLatlng: MutableLiveData<LatLng>? = MutableLiveData()
    var mGoogleMap: MutableLiveData<GoogleMap>? = MutableLiveData()
    var showProgress : ObservableField<Int> = ObservableField()

    init {
        city.set(Utils.search)
        showProgress.set(View.GONE)
    }

    override fun onMapReady(map: GoogleMap?) {
        mGoogleMap?.value = map
    }

    fun initMap(fragmentManager: FragmentManager) {
        val mapFragment = fragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    fun initializingLatLng(latLng: LatLng) {
        mLatlng?.value = latLng
    }

    override fun whetherCallBack(list: MainList) {
        data?.value = list
    }

    override fun onError(cause: String) {

    }

    override fun onCompletedWithError(cause: String) {

    }

    fun invokeSearchClick(view: View) {
        mIMainView?.invokeSearchcallBack(view)
        mIMainView?.showHideWeather(View.GONE, false)
    }

    fun closeWeather(view: View) {
        mIMainView?.showHideWeather(View.GONE, true)
    }

    fun setCityValue(value: String) {
        city.set(value)
    }

    fun setTemparatureValue(value: String) {
        temparature.set(value)
    }

    fun makeWhetherCall(location: LatLng) {
        RestFullServices.makeFeedRequest(this, location)
    }

}