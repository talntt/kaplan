package com.harish.kaplanassignment.ui

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MarkerOptions
import com.harish.kaplanassignment.R
import com.harish.kaplanassignment.base.BaseActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.api.model.Place
import com.harish.kaplanassignment.utils.Utils
import java.util.*
import android.content.Intent
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.Observer
import com.google.android.gms.maps.model.Marker
import com.google.android.libraries.places.api.Places
import com.harish.kaplanassignment.adapter.FeedsAdapter
import com.harish.kaplanassignment.databinding.ActivityMapsBinding
import android.view.animation.AnimationUtils

class MapsActivity : BaseActivity(), IMain.IMainView, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private var viewModel: MapsViewModel? = null
    private var bind: ActivityMapsBinding? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: FeedsAdapter? = null
    private var weatherCard: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        Places.initialize(applicationContext, getString(R.string.google_maps_key))
        Places.createClient(this)
    }

    override fun initViewModel() {
        bind = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        viewModel = ViewModelProviders.of(this).get(MapsViewModel::class.java)
        bind?.viewModel = viewModel
        viewModel?.mIMainView = this
        viewModel?.initMap(supportFragmentManager)
        recyclerView = bind?.recyclerView
        weatherCard = bind?.weatherLayout

        viewModel?.mGoogleMap?.observe(this, Observer {
            mMap = it
            mMap.setOnMarkerClickListener(this)
        })

        viewModel?.mLatlng?.observe(this, Observer {
            mMap.clear()
            mMap.addMarker(MarkerOptions().position(it))
            val location = CameraUpdateFactory.newLatLngZoom(it, Utils.ZOOM)
            mMap.animateCamera(location)
        })


        viewModel?.data?.observe(this, Observer {
            val temp = ((it.listAll?.get(0)?.main!!.humidity?.minus(32))!!.times(5)) / 9
            viewModel?.setTemparatureValue(temp.toString()+"˚c")
            if (adapter == null) {
                adapter = FeedsAdapter(it.listAll!!, this)
                recyclerView?.adapter = adapter
            } else {
                adapter?.updateList(it.listAll!!)
            }
            viewModel?.showProgress?.set(View.GONE)
            showHideWeather(View.VISIBLE, true)
        })

    }

    override fun invokeSearchcallBack(view: View) {

        val fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        ).build(this)
        startActivityForResult(intent, Utils.AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Utils.AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                handlePlaceOnSelected(place)
            }
        }
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        viewModel?.makeWhetherCall(viewModel?.mLatlng?.value!!)
        viewModel?.showProgress?.set(View.VISIBLE)
        return true
    }

    private fun handlePlaceOnSelected(place: Place) {
        viewModel?.setCityValue(place.name!!)
        viewModel?.initializingLatLng(place.latLng!!)
    }

    override fun showHideWeather(value: Int, isAnimate: Boolean) {
        val anim:Int?
        if(value == 0) anim = R.anim.slide_up else anim = R.anim.slide_down
        val slideUp = AnimationUtils.loadAnimation(this, anim)
        weatherCard?.setVisibility(value)
        if(isAnimate) weatherCard?.startAnimation(slideUp)
    }
}
