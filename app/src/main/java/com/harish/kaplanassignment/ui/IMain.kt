package com.harish.kaplanassignment.ui

import android.view.View
import com.harish.kaplanassignment.model.MainList

interface IMain {

    interface IMainView {
        fun invokeSearchcallBack(view: View)
        fun showHideWeather(value: Int, isAnimate: Boolean)
    }
    interface IMainViewModel {
        fun whetherCallBack(list: MainList)
        fun onError(cause: String)
        fun onCompletedWithError(cause: String)
    }
}